# web-and-mobile-archive
An archive of my work from the Web and Mobile 1 and 2 courses in the IST department at Rochester Institute of Technology.
Much of this work was originally written using PHP and server side includes but has since been rewritten in HTML and Javascript to be supported by web hosting that does not allow PHP.
